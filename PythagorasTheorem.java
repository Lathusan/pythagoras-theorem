package com.seven;

import java.util.Scanner;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0 Jan 26, 2021
 **/

public class PythagorasTheorem {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		for (int i = 0; i <= 8; i++) {
			i++;
			for (int j = 1; j <= i; j++) {
				if (i == 5 && j == 1) {
					System.out.print("a *  *  *  *  *");
				}
				if (i == 1 || i == 3 || i == 7 || i == 8 || i == 9) {
					System.out.print("  *");
				}
			}
			System.out.println(" ");
		}

		System.out.print("\nEnter the length a : ");
		double a = scan.nextDouble();

		System.out.println("");
		for (int i = 0; i <= 8; i++) {
			i++;
			for (int j = 1; j <= i; j++) {
				System.out.print("  *");
			}

			System.out.println(" ");
		}

		System.out.println("            b");

		System.out.print("\nEnter the length b : ");
		double b = scan.nextDouble();

		System.out.println("");
		for (int i = 0; i <= 8; i++) {
			i++;
			for (int j = 1; j <= i; j++) {
				System.out.print("  *");
				if (i == 3 && j == 3) {
					System.out.print("        c");
				}
			}

			System.out.println(" ");
		}

		double c = Math.sqrt((Math.pow(a, 2)) + (Math.pow(b, 2)));
		System.out.println("\nThe length of the hypotenuse (c) is : " + c);

	}

}
